import sys, json
from PyQt5 import QtCore, QtWidgets, QtGui

class Staff(object):
    def __init__(self, salarycode = "",
                 name = "",
                 designation = "",
                 department = "",
                 eligibilitydates = [],
                 dateofeligibility = "",
                 presentquarter = ""):
        self.salarycode = salarycode
        self.name = name
        self.designation = designation
        self.department = department
        self.eligibilitydates = eligibilitydates
        self.dateofeligibility = dateofeligibility
        self.presentquarter = presentquarter

class StaffDBEditor(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        super().setWindowTitle("Staff DB Editor")
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        layout = QtWidgets.QVBoxLayout(self._main)

        fileWidget = QtWidgets.QWidget()
        layout.addWidget(fileWidget)
        fileWidgetLayout = QtWidgets.QHBoxLayout(fileWidget)
        openButton = QtWidgets.QPushButton('Open')
        saveButton = QtWidgets.QPushButton('Save...')
        fileWidgetLayout.addWidget(openButton)
        fileWidgetLayout.addWidget(saveButton)

        searchWidget = QtWidgets.QWidget()
        layout.addWidget(searchWidget)
        searchWidgetLayout = QtWidgets.QHBoxLayout(searchWidget)
        self.searchBar = QtWidgets.QLineEdit()
        self.searchBar.textEdited.connect(self.filterText)
        addButton = QtWidgets.QPushButton("Add...")
        addButton.clicked.connect(self.addEntry)
        clearButton = QtWidgets.QPushButton("Clear")
        clearButton.clicked.connect(self.clearSearchBar)
        searchWidgetLayout.addWidget(self.searchBar)
        searchWidgetLayout.addWidget(addButton)
        searchWidgetLayout.addWidget(clearButton)

        self.tableWidget = QtWidgets.QTableWidget()
        self.tableWidget.setRowCount(800)
        self.tableWidget.setColumnCount(6)
        header_labels = ["Salary code", "Name", "Designation", "Department", "DOE", "Present quarter"]
        self.tableWidget.setHorizontalHeaderLabels(header_labels)
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget.itemDoubleClicked.connect(self.editEntry)
        layout.addWidget(self.tableWidget)

        openButton.clicked.connect(self.open_file)
        saveButton.clicked.connect(self.save_file)

        self.stafflist = []

        self.staffEditDialog = QtWidgets.QDialog()
        self.staffEditDialog.resize(400, 400)
        self.staffEditDialog.setModal(True)
        staffEditDialog_layout = QtWidgets.QVBoxLayout(self.staffEditDialog)
        self.staffDialogItems = { "salarycode" : ("Salary Code", QtWidgets.QLineEdit()),
                                  "name" : ("Name", QtWidgets.QLineEdit()),
                                  "designation": ("Designation", QtWidgets.QLineEdit()),
                                  "department" : ("Department", QtWidgets.QLineEdit()),
                                  "dateofeligibility" : ("Date of eligibility", QtWidgets.QLineEdit()),
                                  "presentquarter" : ("Present Qtr.", QtWidgets.QLineEdit())}
        for i in self.staffDialogItems.keys():
            w = QtWidgets.QWidget()
            layout = QtWidgets.QHBoxLayout(w)
            layout.addWidget(QtWidgets.QLabel(self.staffDialogItems[i][0]))
            layout.addWidget(self.staffDialogItems[i][1])
            staffEditDialog_layout.addWidget(w)
        self.dateTableWidget = QtWidgets.QTableWidget()
        self.dateTableWidget.setColumnCount(3)
        self.dateTableWidget.setRowCount(12)
        header_labels = ["Level", "Date", "Inter-se date"]
        self.dateTableWidget.setHorizontalHeaderLabels(header_labels)
        staffEditDialog_layout.addWidget(self.dateTableWidget)
        w = QtWidgets.QWidget()
        layout = QtWidgets.QHBoxLayout(w)
        okButton = QtWidgets.QPushButton("OK")
        layout.addWidget(okButton)
        okButton.clicked.connect(self.dialogOK)
        cancelButton = QtWidgets.QPushButton("Cancel")
        layout.addWidget(cancelButton)
        cancelButton.clicked.connect(self.dialogCancel)
        deleteButton = QtWidgets.QPushButton("Delete")
        layout.addWidget(deleteButton)
        deleteButton.clicked.connect(self.dialogDelete)
        staffEditDialog_layout.addWidget(w)
        self.filterText(self.searchBar.text())

    def clearSearchBar(self):
        self.searchBar.setText("")
        self.filterText(self.searchBar.text())

    def dialogOK(self):
        if not self.staffEditDialog.isVisible():
            return
        i = self.editedItem
        for key in self.staffDialogItems.keys():
            self.stafflist[i].__setattr__(key, self.staffDialogItems[key][1].text())
        self.stafflist[i].eligibilitydates = []
        for j in range(self.dateTableWidget.rowCount()):
            data = [self.dateTableWidget.item(j, 0),
                    self.dateTableWidget.item(j, 1),
                    self.dateTableWidget.item(j, 2)]
            if data[2] is None:
                continue
            data = ["" if x is None else x.text() for x in data]
            if len(data[0]) > 0 or len(data[1]) > 0 or len(data[2]) > 0:
                self.stafflist[i].eligibilitydates.append(data)
        self.staffEditDialog.close()
        self.editedItem = None
        self.stafflist = list(filter(lambda x : len(x.name.strip()) > 0 and len(x.salarycode.strip()) > 0, self.stafflist))
        self.populate_table(self.stafflist)
        self.filterText(self.searchBar.text())

    def dialogCancel(self):
        if self.staffEditDialog.isVisible():
            self.staffEditDialog.close()
            self.editedItem = None

    def dialogDelete(self):
        if self.staffEditDialog.isVisible():
            del self.stafflist[self.editedItem]
            self.staffEditDialog.close()
            self.tableWidget.setRowCount(0)
            self.tableWidget.setRowCount(800)
            self.editedItem = None
            self.populate_table(self.stafflist)

    def addEntry(self):
        self.stafflist.append(Staff())
        self.populate_table(self.stafflist)
        self.editEntry(self.tableWidget.item(len(self.stafflist) - 1, 0))

    def editEntry(self, item):
        s = self.stafflist[item.row()]
        self.staffDialogItems['salarycode'][1].setText(s.salarycode)
        self.staffDialogItems['name'][1].setText(s.name)
        self.staffDialogItems['designation'][1].setText(s.designation)
        self.staffDialogItems['department'][1].setText(s.department)
        self.staffDialogItems['dateofeligibility'][1].setText(s.dateofeligibility)
        self.staffDialogItems['presentquarter'][1].setText(s.presentquarter)
        self.dateTableWidget.clearContents()
        for i in range(len(s.eligibilitydates)):
            self.dateTableWidget.setItem(i, 0, QtWidgets.QTableWidgetItem(s.eligibilitydates[i][0]))
            self.dateTableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem(s.eligibilitydates[i][1]))
            self.dateTableWidget.setItem(i, 2, QtWidgets.QTableWidgetItem(s.eligibilitydates[i][2]))
        self.editedItem = item.row()
        self.staffEditDialog.show()
        self.staffEditDialog.raise_()
        self.staffEditDialog.activateWindow()

    def filterText(self, s):
        for i in range(self.tableWidget.rowCount()):
            scode = self.tableWidget.item(i, 0)
            name = self.tableWidget.item(i, 1)
            if scode and s in scode.text() or name and s in name.text():
                self.tableWidget.setRowHidden(i, False)
            else:
                self.tableWidget.setRowHidden(i, True)

    def open_file(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","JSON Files (*.json);;All Files (*)", options=options)
        if fileName:
            self.stafflist = []
            self.tableWidget.setRowCount(0)
            self.tableWidget.setRowCount(800)
            f = open(fileName, 'r')
            data = json.load(f)
            f.close()
            for i in data['data']:
                if 'dateofeligibility' in i:
                    doe = i['dateofeligibility']
                else:
                    doe = None
                self.stafflist.append(Staff(salarycode = i['salarycode'],
                                            name=i['name'],
                                            designation=i['designation'],
                                            department=i['department'],
                                            eligibilitydates=i['eligibilitydates'],
                                            dateofeligibility=doe,
                                            presentquarter=i['presentquarter']))

            self.searchBar.setText("")
            self.populate_table(self.stafflist)
            self.filterText("")

    def save_file(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"QFileDialog.getOpenFileName()", "","JSON Files (*.json);;All Files (*)", options=options)
        if fileName:
            f = open(fileName, 'w')
            data = { 'data' : [] }
            for i in self.stafflist:
                idict = i.__dict__
                if not 'dateofeligibility' in idict or i.dateofeligibility and len(i.dateofeligibility.strip()) < 1:
                    idict.pop('dateofeligibility', None)
                data['data'].append(idict)
            json.dump(data, f, indent=True)
            f.close()

    def populate_table(self, staffs):
        for i in range(len(staffs)):
            doe = ""
            if 'dateofeligibility' in staffs[i].__dict__:
                doe = staffs[i].dateofeligibility
            self.tableWidget.setItem(i, 0, QtWidgets.QTableWidgetItem(staffs[i].salarycode))
            self.tableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem(staffs[i].name))
            self.tableWidget.setItem(i, 2, QtWidgets.QTableWidgetItem(staffs[i].designation))
            self.tableWidget.setItem(i, 3, QtWidgets.QTableWidgetItem(staffs[i].department))
            self.tableWidget.setItem(i, 4, QtWidgets.QTableWidgetItem(doe))
            self.tableWidget.setItem(i, 5, QtWidgets.QTableWidgetItem(staffs[i].presentquarter))

if __name__ == "__main__":
    qapp = QtWidgets.QApplication(sys.argv)
    app = StaffDBEditor()
    app.show()
    qapp.exec()
