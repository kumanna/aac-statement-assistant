module Staff exposing (LevelDate, PrefStruct, Staff, getNonFacultyOrderedDate, getStaffOrderedDate, staffListDecoder, staffMoreInfoNeeded)

import Json.Decode exposing (Decoder, field, index, list, map2, map3, maybe, string, succeed)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)


type alias LevelDate =
    { level : String, joindate : String, intersedate : String }


type alias PrefStruct =
    { preferences : List String, remarks : Maybe String }


type alias Staff =
    { salarycode : String
    , name : String
    , designation : String
    , department : String
    , eligibilitydates : List LevelDate
    , dateofeligibility : Maybe String
    , presentquarter : String
    , preferences : Maybe PrefStruct
    , moreInfoNeeded : Bool
    }


levelDateDecoder : Decoder LevelDate
levelDateDecoder =
    map3 LevelDate
        (index 0 string)
        (index 1 string)
        (index 2 string)


prefDecoder : Decoder PrefStruct
prefDecoder =
    map2 PrefStruct
        (field "preferences" (list string))
        (field "remarks" (maybe string))


staffListDecoder : Decoder (List Staff)
staffListDecoder =
    field "data"
        (list
            (succeed
                Staff
                |> required "salarycode" string
                |> required "name" string
                |> required "designation" string
                |> required "department" string
                |> required "eligibilitydates" (list levelDateDecoder)
                |> optional "dateofeligibility" (maybe string) Maybe.Nothing
                |> required "presentquarter" string
                |> optional "preferences" (maybe prefDecoder) Maybe.Nothing
                |> hardcoded False
            )
        )


dateAlphaOrder : String -> String
dateAlphaOrder dateStr =
    String.join ""
        [ String.slice 6 10 dateStr
        , String.slice 3 5 dateStr
        , String.slice 0 2 dateStr
        ]


getStaffOrderedDate : Staff -> String
getStaffOrderedDate s =
    String.join ""
        [ dateAlphaOrder (Maybe.withDefault "31-12-2199" s.dateofeligibility) ]


getNonFacultyOrderedDate : Staff -> String
getNonFacultyOrderedDate s =
    s.eligibilitydates
        |> List.map (\n -> n.intersedate)
        |> List.map dateAlphaOrder
        |> String.join ""


nullStaff : Staff
nullStaff =
    { salarycode = ""
    , name = ""
    , designation = ""
    , department = ""
    , eligibilitydates = []
    , dateofeligibility = Maybe.Nothing
    , presentquarter = ""
    , preferences = Maybe.Nothing
    , moreInfoNeeded = False
    }


type alias StaffDOBHelper =
    { previous : Staff
    , current : Staff
    , next : Staff
    }


staffMoreInfoNeeded : Bool -> List Staff -> List Staff
staffMoreInfoNeeded non_faculty_mode staffList =
    staffList
        |> (\n -> [ nullStaff ] ++ n ++ [ nullStaff ])
        |> (\n -> List.map3 (\a b c -> StaffDOBHelper a b c) n (List.drop 1 n) (List.drop 2 n))
        |> List.map
            (\n ->
                let
                    current =
                        n.current

                    previous =
                        n.previous

                    next =
                        n.next
                in
                if not non_faculty_mode then
                    if current.dateofeligibility == previous.dateofeligibility || current.dateofeligibility == next.dateofeligibility then
                        { current | moreInfoNeeded = True }

                    else
                        current

                else
                    { current | eligibilitydates = levelDateHelper previous.eligibilitydates current.eligibilitydates next.eligibilitydates [] }
            )


levelDateHelper : List LevelDate -> List LevelDate -> List LevelDate -> List LevelDate -> List LevelDate
levelDateHelper previous current next running_list =
    let
        extract_elig_date edate =
            case List.head edate of
                Just x ->
                    dateAlphaOrder x.intersedate |> Maybe.Just

                Nothing ->
                    Nothing

        prev_ldate =
            extract_elig_date previous

        cur_ldate =
            extract_elig_date current

        next_ldate =
            extract_elig_date next

        current_list_element =
            List.head current
    in
    case current_list_element of
        Just l ->
            if cur_ldate == prev_ldate || cur_ldate == next_ldate then
                levelDateHelper
                    (if cur_ldate == prev_ldate then
                        List.tail previous |> Maybe.withDefault []

                     else
                        []
                    )
                    (List.tail current |> Maybe.withDefault [])
                    (if cur_ldate == next_ldate then
                        List.tail next |> Maybe.withDefault []

                     else
                        []
                    )
                    (running_list ++ [ l ])

            else
                running_list ++ [ l ]

        Nothing ->
            running_list
