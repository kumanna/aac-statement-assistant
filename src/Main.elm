module Main exposing (Model, Msg(..), init, main, subscriptions, update, view)

import Browser
import File exposing (File)
import File.Select as Select
import Html exposing (Html, br, button, div, h1, h2, input, label, li, p, pre, strong, table, tbody, td, text, textarea, th, thead, tr, ul)
import Html.Attributes exposing (attribute, class, name, placeholder, scope, style, type_, value, width)
import Html.Events exposing (onCheck, onClick, onInput)
import Http
import Json.Decode
import Staff exposing (PrefStruct, Staff, getNonFacultyOrderedDate, getStaffOrderedDate, staffListDecoder, staffMoreInfoNeeded)
import Task
import Set


-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type State
    = Failure Http.Error
    | Loading
    | LoadedDB (List Staff)
    | NoDBLoaded


type ParsedPreference
    = InvalidEntry (Maybe String)
    | SalaryCodeNotFound
    | QuartersIncorrect String (List String)
    | QuartersIncorrectWithRemark String (List String) String
    | ValidPreference String (List String)
    | ValidPreferenceWithRemark String (List String) String


type alias Model =
    { state : State
    , dbURL : String
    , quarters : List String
    , raw_preferences : String
    , parsed_preferences : List ParsedPreference
    , raw_quarter_list : String
    , non_faculty_mode : Bool
    , show_allotted_status : Bool
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { state = NoDBLoaded
      , dbURL = ""
      , quarters = []
      , raw_preferences = ""
      , parsed_preferences = []
      , raw_quarter_list = ""
      , non_faculty_mode = False
      , show_allotted_status = False
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = GotDB (Result Http.Error (List Staff))
    | URLChanged String
    | LoadNewDB
    | QuartersChanged String
    | PreferenceTextUpdate String
    | UpdatePreferences
    | ClearPreferences
    | DBUploadRequested
    | DBUploadSelected File
    | DBUploadLoaded String
    | PrefsUploadRequested
    | PrefsUploadSelected File
    | PrefsUploadLoaded String
    | CheckedNonFacultyMode Bool
    | CheckedAllottedStatus Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        split_quarters_string =
            \quarters_string -> String.split "," quarters_string |> List.filter (\m -> String.length m > 0)
    in
    case msg of
        LoadNewDB ->
            ( model
            , Http.get
                { url = model.dbURL
                , expect = Http.expectJson GotDB staffListDecoder
                }
            )

        URLChanged s ->
            ( { model | dbURL = s }, Cmd.none )

        GotDB result ->
            case result of
                Ok staffData ->
                    ( if List.length model.parsed_preferences == 0 then
                        { model | state = LoadedDB staffData }

                      else
                        parsePreferences { model | state = LoadedDB staffData, parsed_preferences = [] }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | state = Failure err }, Cmd.none )

        QuartersChanged quarters_string ->
            ( { model
                | quarters = split_quarters_string quarters_string
                , raw_quarter_list = quarters_string
              }
            , Cmd.none
            )

        PreferenceTextUpdate s ->
            ( { model | raw_preferences = s }, Cmd.none )

        UpdatePreferences ->
            ( parsePreferences model, Cmd.none )

        ClearPreferences ->
            ( { model | parsed_preferences = [], raw_preferences = "" }, Cmd.none )

        DBUploadRequested ->
            ( model, Select.file [ "text/json" ] DBUploadSelected )

        DBUploadSelected file ->
            ( model
            , Task.perform DBUploadLoaded (File.toString file)
            )

        DBUploadLoaded content ->
            case Json.Decode.decodeString staffListDecoder content of
                Ok staffData ->
                    ( if List.length model.parsed_preferences == 0 then
                        { model | state = LoadedDB staffData }

                      else
                        parsePreferences { model | state = LoadedDB staffData, parsed_preferences = [] }
                    , Cmd.none
                    )

                _ ->
                    ( model
                    , Cmd.none
                    )

        PrefsUploadRequested ->
            ( model, Select.file [ "text/csv", "text/plain" ] PrefsUploadSelected )

        PrefsUploadSelected file ->
            ( model
            , Task.perform PrefsUploadLoaded (File.toString file)
            )

        PrefsUploadLoaded content ->
            -- Check whether the first line has the preferences
            case String.split "\n" content of
                a :: rest ->
                    let
                        trimmed =
                            String.trim a

                        trimmed_and_replaced =
                            String.trim (String.replace "quarters:" "" trimmed)
                    in
                    if String.startsWith "quarters:" trimmed then
                        ( parsePreferences
                            { model
                                | raw_preferences = String.join "\n" rest
                                , raw_quarter_list = trimmed_and_replaced
                                , quarters = split_quarters_string trimmed_and_replaced
                            }
                        , Cmd.none
                        )

                    else
                        ( parsePreferences { model | raw_preferences = content }, Cmd.none )

                _ ->
                    ( parsePreferences { model | raw_preferences = content }, Cmd.none )

        CheckedNonFacultyMode nonfacmode ->
            ( { model | non_faculty_mode = nonfacmode }, Cmd.none )

        CheckedAllottedStatus astatus ->
            ( { model | show_allotted_status = astatus }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ h1 [] [ text "AAC Allotment Helper" ]
        , div [ class "row" ]
            [ div [ class "col-sm border" ]
                [ h2 [] [ text "Step 1: Load database" ]
                , label [ attribute "for" "jsondb" ] [ text ("JSON database" ++ ": ") ]
                , input
                    [ placeholder "Enter JSON database URL here..."
                    , name "jsondb"
                    , style "width" "20em"
                    , onInput URLChanged
                    ]
                    []
                , br [] []
                , input [ type_ "checkbox", onCheck CheckedNonFacultyMode ] []
                , text "Non-Faculty mode"
                , br [] []
                , button [ class "btn btn-primary", onClick LoadNewDB ] [ text "Load database" ]
                , button [ class "btn btn-primary", onClick DBUploadRequested ] [ text "Load database from file" ]
                , br [] []
                ]
            ]
        , case model.state of
            LoadedDB mydb ->
                div [ class "row" ]
                    [ div [ class "col-sm border" ]
                        [ h2 [] [ text "Step 2: Specify quarters for allotment" ]
                        , strong [] [ text "Note: " ]
                        , text "If the preferences file starts with a \"quarters:\" line, this can be left blank."
                        , br [] []
                        , label [ attribute "for" "jsondb" ] [ text ("Quarters" ++ ": ") ]
                        , input
                            [ placeholder "Enter comma separated quarters..."
                            , name "jsondb"
                            , style "width" "20em"
                            , onInput QuartersChanged
                            , value model.raw_quarter_list
                            ]
                            []
                        ]
                    ]

            _ ->
                br [] []
        , case model.state of
            LoadedDB staffList ->
                div [ class "row" ]
                    [ div [ class "col-sm border" ]
                        [ h2 [] [ text "Step 3: Specify preferences" ]
                        , p []
                            [ strong [] [ text "Note: " ]
                            , text "If the preferences file starts with a \"quarters: \" line, the quarters in Step 2 will be replaced!"
                            ]
                        , textarea
                            [ placeholder "Provide preference data here."
                            , onInput PreferenceTextUpdate
                            , value model.raw_preferences
                            , style "width" "40em"
                            ]
                            []
                        , br [] []
                        , button [ class "btn btn-primary", onClick UpdatePreferences ] [ text "Update preferences" ]
                        , button [ class "btn btn-primary", onClick PrefsUploadRequested ] [ text "Load preferences from file" ]
                        , button [ class "btn btn-primary", onClick ClearPreferences ] [ text "Clear preferences" ]
                        , br [] []
                        , input [ type_ "checkbox", onCheck CheckedAllottedStatus ] []
                        , text "Show allotted status"
                      , ul []
                            (model.parsed_preferences
                                |> List.map
                                    (\n ->
                                        case n of
                                            QuartersIncorrect q l ->
                                                Maybe.Just ("Incorrect quarters: " ++ q ++ " " ++ String.join ", " l)

                                            QuartersIncorrectWithRemark q l _ ->
                                                Maybe.Just ("Incorrect quarters: " ++ q ++ " " ++ String.join ", " l)

                                            InvalidEntry err ->
                                                case err of
                                                    Just x ->
                                                        if String.length (String.trim x) > 0 then
                                                            Maybe.Just
                                                                ("Error in entry "
                                                                    ++ x
                                                                )

                                                        else
                                                            Maybe.Nothing

                                                    _ ->
                                                        Maybe.Nothing

                                            _ ->
                                                Maybe.Nothing
                                    )
                                |> List.filter
                                    (\n ->
                                        case n of
                                            Just x ->
                                                True

                                            Nothing ->
                                                False
                                    )
                                |> List.map
                                    (\n -> li [] [ Maybe.withDefault "" n |> text ])
                            )
                        ]
                    ]

            _ ->
                br [] []
        , div [ class "row" ]
            [ case model.state of
                NoDBLoaded ->
                    br [] []

                Failure err ->
                    case err of
                        Http.BadUrl s ->
                            text ("I was unable to load your database. Please check the url " ++ s ++ "!")

                        Http.BadBody s ->
                            div []
                                [ text "I was unable to load your database. Please check the JSON content:"
                                , pre [] [ text s ]
                                ]

                        _ ->
                            text "I was unable to load your database due to network errors."

                Loading ->
                    text "Loading..."

                LoadedDB fullContent ->
                    let
                        quarters_set = Set.fromList model.quarters
                        sortedContent = fullContent
                                |> List.filter
                                    (\n ->
                                        if List.length model.parsed_preferences > 0 then
                                            case n.preferences of
                                                Just _ ->
                                                    True

                                                Nothing ->
                                                    False

                                        else
                                            True
                                    )
                                |> List.sortBy
                                    (if model.non_faculty_mode then
                                        getNonFacultyOrderedDate

                                     else
                                        getStaffOrderedDate
                                    )
                                |> staffMoreInfoNeeded model.non_faculty_mode
                        highlights = quartersToHighlight (List.map .preferences sortedContent) [] (Set.fromList model.quarters)
                    in
                    div [ class "col-sm border" ]
                        [ h2 [] [ text "Result" ]
                        , p [] [ text ("Comparative Statement for " ++ String.join ", " model.quarters) ]
                        , table [ class "table table-bordered table-striped" ]
                            ([ [ thead []
                                    [ getTableHeaderRow model.non_faculty_mode ]
                               ]
                             , List.map2 Tuple.pair sortedContent highlights
                                |> List.indexedMap Tuple.pair
                                |> List.map (\n -> getTableStaffRow model.non_faculty_mode (1 + Tuple.first n) (List.length model.parsed_preferences > 0) (Tuple.first (Tuple.second n)) (Tuple.second (Tuple.second n)) model.show_allotted_status)
                             ]
                                |> List.concat
                            )
                        ]
            ]
        ]


getTableHeaderRow : Bool -> Html msg
getTableHeaderRow non_faculty_mode =
    tr []
        [ th [ scope "col" ] [ text "Sr. No." ]
        , th [ scope "col" ] [ text "Salary code" ]
        , th [ scope "col" ] [ text "Name, Designation, Department" ]
        , if not non_faculty_mode then
            th [ scope "col", width 200 ] [ text "Date of AP/ASP/P" ]

          else
            text ""
        , if not non_faculty_mode then
            th [ scope "col" ] [ text "Date of Inter-se-seniority" ]

          else
            text ""
        , th [ scope "col" ] [ text "Date of eligibility" ]
        , th [ scope "col" ] [ text "Preference" ]
        , th [ scope "col" ] [ text "Present Qtr. No." ]
        , th [ scope "col" ] [ text "Remarks" ]
        ]


getTableStaffRow : Bool -> Int -> Bool -> Staff -> Maybe String -> Bool -> Html msg
getTableStaffRow non_faculty_mode sno use_preferences { salarycode, name, designation, department, eligibilitydates, dateofeligibility, presentquarter, preferences, moreInfoNeeded } highlight showHighlight =
    let
        prefstr =
            if use_preferences then
                case preferences of
                    Just x ->
                        if True then
                            String.join ", " x.preferences

                        else
                            ""

                    Nothing ->
                        ""

            else
                ""

        remarks =
            if use_preferences then
                case preferences of
                    Just x ->
                        Maybe.withDefault "" x.remarks

                    Nothing ->
                        ""

            else
                ""
    in
    tr []
        [ td [ scope "row" ] [ sno |> String.fromInt |> text ]
        , td [] [ text salarycode ]
        , td [] [ [ name, designation, department ] |> String.join ", " |> text ]
        , if not non_faculty_mode then
            td []
                (eligibilitydates
                    |> List.map (\n -> n.joindate ++ " (" ++ n.level ++ ")")
                    |> List.map text
                    |> List.intersperse (br [] [])
                )

          else
            text ""
        , if not non_faculty_mode then
            td []
                (eligibilitydates
                    |> List.map
                        (\n ->
                            if n.joindate /= n.intersedate then
                                n.intersedate

                            else
                                ""
                        )
                    |> List.map text
                    |> List.intersperse (br [] [])
                )

          else
            td []
                (eligibilitydates
                    |> List.map (\n -> n.intersedate)
                    |> List.map text
                    |> List.intersperse (br [] [])
                )
        , if not non_faculty_mode then
            td []
                [ if moreInfoNeeded then
                    case dateofeligibility of
                        Just x ->
                            strong [] [ x ++ " (Date information needed)" |> text ]

                        Nothing ->
                            strong [] [ "Not eligible" |> text ]

                  else
                    case dateofeligibility of
                        Just x ->
                            x |> text

                        Nothing ->
                            strong [] [ "Not eligible" |> text ]
                ]

          else
            text ""
        , td []
            ([ if String.startsWith "Incorrect" prefstr then
                strong [] [ prefstr |> text ]

              else
                prefstr |> text
            ] ++ (case highlight of
                      Just y ->
                          if showHighlight then
                              [ strong [] [ " (maybe allotted " ++ y ++ ")" |> text ] ]
                          else
                              []
                      Nothing ->
                          []))
        , td [] [ presentquarter |> text ]
        , td [] [ remarks |> text ]
        ]


parsePreferences : Model -> Model
parsePreferences model =
    let
        staffList =
            case model.state of
                LoadedDB s ->
                    s |> List.map (\n -> { n | preferences = Maybe.Nothing })

                _ ->
                    []

        parsed_preferences =
            String.split "\n" model.raw_preferences
                |> List.map (String.split ",")
                |> List.map (List.map String.trim)
                |> List.map
                    (\n ->
                        case n of
                            [] ->
                                InvalidEntry Maybe.Nothing

                            [ a ] ->
                                InvalidEntry (Maybe.Just a)

                            a :: b ->
                                if List.member a (List.map .salarycode staffList) then
                                    if List.all (\q -> List.member q model.quarters || String.startsWith "\"" q) b then
                                        ValidPreference a b

                                    else
                                        QuartersIncorrect a b

                                else
                                    InvalidEntry (Maybe.Just (String.join "," (a :: b)))
                    )
                |> List.map
                    (\n ->
                        case n of
                            ValidPreference a b ->
                                case List.head (List.reverse b) of
                                    Just x ->
                                        if String.startsWith "\"" x && String.endsWith "\"" x && List.length b > 1 then
                                            ValidPreferenceWithRemark a (List.take (List.length b - 1) b) (String.replace "\"" "" x)

                                        else if String.startsWith "\"" x && String.endsWith "\"" x && List.length b <= 1 then
                                            InvalidEntry (Maybe.Just (String.join "," (a :: b)))

                                        else
                                            n

                                    Nothing ->
                                        InvalidEntry (Maybe.Just (String.join "," (a :: b)))

                            QuartersIncorrect a b ->
                                case List.head (List.reverse b) of
                                    Just x ->
                                        if String.startsWith "\"" x && String.endsWith "\"" x && List.length b > 1 then
                                            QuartersIncorrectWithRemark a (List.take (List.length b - 1) b) (String.replace "\"" "" x)

                                        else if String.startsWith "\"" x && String.endsWith "\"" x && List.length b <= 1 then
                                            QuartersIncorrect a b

                                        else
                                            QuartersIncorrect a b

                                    Nothing ->
                                        QuartersIncorrect a b

                            _ ->
                                n
                    )

        newStaffList =
            staffList |> List.map (getParsedPreference parsed_preferences)
    in
    { model
        | parsed_preferences = parsed_preferences
        , state = LoadedDB newStaffList
    }


getParsedPreference : List ParsedPreference -> Staff -> Staff
getParsedPreference parsed_preferences staff =
    parsed_preferences
        |> List.filter
            (\n ->
                case n of
                    ValidPreference a _ ->
                        a == staff.salarycode

                    ValidPreferenceWithRemark a _ _ ->
                        a == staff.salarycode

                    QuartersIncorrect a _ ->
                        a == staff.salarycode

                    QuartersIncorrectWithRemark a _ _ ->
                        a == staff.salarycode

                    _ ->
                        False
            )
        |> (\n ->
                case List.head n of
                    Just x ->
                        case x of
                            ValidPreference _ b ->
                                { staff | preferences = Maybe.Just { preferences = b, remarks = Maybe.Nothing } }

                            ValidPreferenceWithRemark _ b c ->
                                { staff | preferences = Maybe.Just { preferences = b, remarks = Maybe.Just c } }

                            QuartersIncorrect _ l ->
                                { staff | preferences = Maybe.Just { preferences = [ "Incorrect preferences: " ++ String.join ", " l ], remarks = Maybe.Nothing } }

                            QuartersIncorrectWithRemark _ l r ->
                                { staff | preferences = Maybe.Just { preferences = [ "Incorrect preferences: " ++ String.join ", " l ], remarks = Just r } }

                            _ ->
                                staff

                    Nothing ->
                        staff
           )

-- quartersToHighlight: List Staff -> List (Maybe String) -> (Set Int) -> (List Staff, List (Maybe String), Set Int)
quartersToHighlight staffList runningChoices choiceSet =
    case staffList of
        [] -> runningChoices
        [a] -> case ((List.filter (\n -> Set.member n choiceSet) (case a of
                                                                    Just x ->
                                                                        x.preferences
                                                                    Nothing ->
                                                                        [])) |> List.head) of
                  Just x ->
                      runningChoices ++ [Just x]
                  Nothing ->
                      runningChoices ++ [Nothing]
        a::tail -> case ((List.filter (\n -> Set.member n choiceSet) (case a of
                                                                    Just x ->
                                                                        x.preferences
                                                                    Nothing ->
                                                                        [])) |> List.head) of
                  Just x ->
                       quartersToHighlight tail (runningChoices ++ [Just x]) (Set.remove x choiceSet)
                  Nothing ->
                       quartersToHighlight tail (runningChoices ++ [Nothing]) choiceSet
